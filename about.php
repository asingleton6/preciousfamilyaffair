<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>A Precious Affair of Families</title>

  <?php include_once("header.php");?>
  <!-- Wrap all page content for sticky footer to work -->
  <div id="wrap">
    <!-- Page content -->
    <div class="container adjust-pad">
      <div class="row">
        <div class="col-lg-4 col-sm-2"></div>
        <div class="col-lg-4 col-sm-8 title"><h1>ABOUT US</h1></div>
        <div class="col-lg-4 col-sm-2"></div>
      </div>
      <div class="row">
        <div class="col-lg-6 col-md-12">
            <img class="about-img img-circle img-responsive" width="600" src="../img/about/alicia-g.jpeg">
            <p class="name-about alicia-font">Alicia S. Gordon</p>
            <p class="staff-title">OWNER & OPERATOR</p>
        </div>
        <div class="col-sm-2"></div>
        <div class="col-lg-6 col-sm-8">
          <div class="about-line-height">
            <p><span class="larger">Hello,</span> I am the owner and operator of “A Precious Affair of Families!” 
            I am family oriented!  I am a daughter, a sister, a mom, grandma, the list goes on!  I attended Georgia Military College and Georgia State University, majoring in Criminal Justice. I am a graduate of Le Cordon Bleu Culinary School. I have been in the Customer Service industry for many years, as long as I could work.  I have been providing a million and one services from selling dental insurance, Avon, vacuum cleaners, perfumes, magazines, security systems and pizza discount cards to working in restaurants and for different security companies. Even though I worked for many companies and supervised many people at any given time, I always wanted my own business.  So with all the knowledge that I have obtained over the years, I decided to put all my talents together and come with a way to use them all.</p>
            <p>I have had other smaller companies that would fit one category and I would work on at that time.  Then I would give up and stop for a little while saying this may not be for me or allow other people and things to come in my life and lose focus of myself.  This time it is different! I have a goal, I have a focus, and I have a plan! I am moving forward to my future!</p>
            <h4 class="tag-about">Be blessed from my family to yours.</h4>
          </div>
          <h2 class="staff-heading">TEAM MEMBERS</h2>
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
              <img class="about-img img-circle img-responsive" width="270" src="../img/about/1347.jpeg">
              <p class="name-about">Keonna Gordon</p>
              <p class="staff-title">STAFF</p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <img class="about-img img-circle img-responsive" width="270" src="../img/about/1439.jpeg">
              <p class="name-about">Narvechia Hines-Stephens</p>
              <p class="staff-title">STAFF</p>
            </div>
          </div>
        </div>
        <div class="col-sm-2"></div>
      </div>
    </div>
    <div id="push"></div>
  <?php include_once("footer.php");?>