<?php 
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>
<?php 
// Run a select query to get my letest 6 items
// Connect to the MySQL database  
include "storescript/connect_to_mysql.php"; 
$dynamic_list = "";
$sql = mysql_query("SELECT * FROM products WHERE category='Catering' AND sub_category='Baked Goods' ORDER BY date_added DESC LIMIT 9");
$productCount = mysql_num_rows($sql); // count the output amount
$i = 0;

if ($productCount > 0) {
  $dynamic_list = '<table class="table">';
  while($row = mysql_fetch_array($sql)){ 
    $id = $row["id"];
    $product_name = $row["product_name"];
    $price = $row["price"];
    $date_added = strftime("%b %d, %Y", strtotime($row["date_added"]));
    if ($i % 3 == 0) {
      $dynamic_list .= 
      '<tr>
        <td>
        <a href="product.php?id=' . $id . '">
          <img src="inventory_images/' . $id . '.jpg" alt="' . $product_name . '" width="200" height="210" border="1" />
        </a><br/>
        ' . $product_name . '<br />
        $' . $price . '<br />
        <a href="product.php?id=' . $id . '">View Product Details</a>
        </td>';
    } else {
      $dynamic_list .= 
      '<td>
        <a href="product.php?id=' . $id . '">
          <img src="inventory_images/' . $id . '.jpg" alt="' . $product_name . '" width="200" height="210" border="1" />
        </a><br/>
        ' . $product_name . '<br />
        $' . $price . '<br />
        <a href="product.php?id=' . $id . '">View Product Details</a>
        </td>';
    }
    $i++; 
  }
  $dynamic_list .= '</tr></table>';
} else {
  $dynamic_list = "We have no products listed in our store yet";
}
mysql_close();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>A Precious Affair of Families</title>

  <?php include_once("header.php");?>
  <!-- Wrap all page content for sticky footer to work -->
  <div id="wrap">
    <!-- Page content -->
    <div class="container adjust-width-others">
      
        <?php echo $dynamic_list; ?>
      
    </div>
    <div id="push"></div>
  <?php include_once("footer.php");?>