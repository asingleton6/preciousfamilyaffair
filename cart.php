<?php
//Start session first thing in script
session_start(); 
error_reporting(E_ALL);
ini_set('display_errors', '1');
include "storescript/connect_to_mysql.php"; 
?>
<?php
//If user attempts to add something to the cart from the product page 
if (isset($_POST['pid'])) {
  $pid = $_POST['pid'];
  $was_found = false;
  $i = 0;
  //If the cart session variable is not set or cart array is empty
  if(!isset($_SESSION['cart_array']) || count($_SESSION["cart_array"]) < 1) {
    //Run if the cart is empty or not set
    $_SESSION["cart_array"] = array(0 => array("item_id" => $pid, "quantity" => 1));
  } else {
    //Run if the cart has at least one item in it
    foreach ($_SESSION["cart_array"] as $each_item) {
      $i++;
      while (list($key, $value) = each($each_item)) {
        if($key == "item_id" && $value == $pid) {
          //That item is in cart already so let's adjust its quantity using array_splice().
          array_splice($_SESSION["cart_array"], $i-1, 1, array(array("item_id" => $pid, "quantity" => $each_item['quantity'] + 1)));
          $was_found = true;
        }
      }
    }
    if($was_found == false) {
      array_push($_SESSION['cart_array'], array("item_id" => $pid, "quantity" => 1));
    }
  }
  header("location: cart.php");
  exit();
}
?>
<?php  
//Empty shopping cart
if (isset($_GET['cmd']) && $_GET['cmd'] == "emptycart") {
  unset($_SESSION["cart_array"]);
}
?>
<?php
//Allows the user to remove item from the cart  
if(isset($_POST["index_to_remove"]) && $_POST["index_to_remove"]!="") {
  //Access the array and run code to remove that array index
  $key_to_remove = $_POST["index_to_remove"];
  if(count($_SESSION["cart_array"]) <= 1){
    unset($_SESSION["cart_array"]);
  } else {
    unset($_SESSION["cart_array"]["$key_to_remove"]);
    sort($_SESSION["cart_array"]);
  }
}
?>
<?php 
//Allows the user to adjust item quantity
if(isset($_POST["index_to_adjust"]) && $_POST["index_to_adjust"]!="") {
  $item_to_adjust = $_POST["index_to_adjust"];
  $quantity = $_POST["quantity"];
  $quantity = preg_replace('#[^0-9]#i', '', $quantity); // filter everything but numbers
  if ($quantity >= 100) { $quantity = 99; }
  if ($quantity < 1) { $quantity = 1; }
  if ($quantity == "") { $quantity = 1; }
  $i = 0;
  foreach ($_SESSION["cart_array"] as $each_item) {
    $i++;
    while (list($key, $value) = each($each_item)) {
      if($key == "item_id" && $value == $item_to_adjust) {
        //That item is in cart already so let's adjust its quantity using array_splice().
        array_splice($_SESSION["cart_array"], $i-1, 1, array(array("item_id" => $item_to_adjust, "quantity" => $quantity)));
      }
    }
  }
}
?>
<?php  
//Render the cart
$cart_output = "";
$cart_total = "";
if(!isset($_SESSION["cart_array"]) || count($_SESSION["cart_array"]) < 1) {
  $cart_output = "<h2>Your shopping cart is empty</h2>";
} else {
    $i = 0;
    foreach ($_SESSION["cart_array"] as $each_item){
      $item_id = $each_item['item_id'];
      $sql = mysql_query("SELECT * FROM products WHERE id='$item_id' LIMIT 1");
      while ($row = mysql_fetch_array($sql)) {
        $product_name = $row["product_name"];
        $price = $row["price"];
        $details = $row["details"];
      }
      $price_total = $price * $each_item['quantity'];
      $cart_total = $price_total + $cart_total;
      setlocale(LC_MONETARY, "en_US");
      $price_total = money_format("%10.2n", $price_total);

      //Dynamic table row
      $cart_output .= "<tr> ";
      $cart_output .= '<td>
                      <div class="col-lg-4">
                        <img class="inventory-image" src="inventory_images/' .$item_id. '.jpg" alt="'.$product_name.'" width="100" height="110" />
                      </div>
                      <div class="col-lg-8">
                      <h4>' .$product_name. '</h4>
                      </div>
                      </td>';
      //$cart_output .= '<td><h5> ' .$details. ' </h5></td>';
      $cart_output .= '<td> $' .$price. '</td>';
      $cart_output .= '<td>
                      <form class="form-group" action="cart.php" method="post">
                      <input class="form-control" name="quantity" type="text" value="'.$each_item['quantity'].'" size="5" maxlength="2">
                      <button class="update-btn btn-default" name="adjust_btn' . $item_id . '" type="submit">Update</button>
                      <input name="index_to_adjust" type="hidden" value="' . $item_id . '" />
                      </form>
                      </td>';
      $cart_output .= '<td>' .$price_total. '</td>';
      $cart_output .= '<td>
                      <form action="cart.php" method="post">
                      <button class="clear-btn" name="delete_btn' . $item_id . '" type="submit">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                      </button>
                      <input name="index_to_remove" type="hidden" value="' . $i . '" />
                      </form></td>';
      $cart_output .= '</tr> ';
      $i++;
    }
    setlocale(LC_MONETARY, "en_US");
    $cart_total = money_format("%10.2n", $cart_total);
    $cart_total = "Total: ".$cart_total;
  }
?> 
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Shopping cart</title>

  <?php include_once("header.php");?>
  <!-- Wrap all page content for sticky footer to work -->
  <div id="wrap">
    <!-- Page content -->
    <div class="container adjust-pad cart">
      <form action="cart.php" enctype="multipart/form-data" name="my_form" id="my_form" method="post">
        <h1>Shopping Cart</h1>
        <div class="table-responsive">
          <table class="table table-hover table-condensed">
            <thead>
              <tr class="table-header">
                <th style="width:45%">Product</th>
                <!-- <td>Product Description</td> -->
                <th style="width:8%"">Price</th>
                <th style="width:25%">Quantity</th>
                <th style="width:10%">Subtotal</th>
                <th style="width:12%"> </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <?php echo $cart_output; ?>
              </tr>
            </tbody>
            <tfoot>
              <tr class="table-footer">
                <td></td>
                <td></td>
                <td></td>
                <td class="cart-total"><?php echo $cart_total; ?></td>
                <td><a class="empty-cart" href="cart.php?cmd=emptycart"><i class="fa fa-trash" aria-hidden="true"></i> Clear Cart</a></td>
              </tr>
            </tfoot>
          </table>
        </div>
      </form>
    </div>
    <div id="push"></div>
  <?php include_once("footer.php");?>