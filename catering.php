<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>A Precious Affair of Families</title>

  <?php include_once("header.php");?>
  <!-- Wrap all page content for sticky footer to work -->
  <div id="wrap">
    <!-- Page content -->
    <div class="container adjust-pad catering">
      <div class="row">
        <div class="col-lg-4 col-sm-2"></div>
        <div class="col-lg-4 col-sm-8 title"><h1>CATERING</h1></div>
        <div class="col-lg-4 col-sm-2"></div>
      </div>
      <div class="row services">
        <div class="white-background col-lg-3 col-md-3 col-sm-3 col-xs-12"> <!-- required for floating -->
          <!-- Nav tabs -->
          <ul class="nav nav-tabs tabs-left">
            <li class="active"><a href="#precious-family" data-toggle="tab"><h4>Precious Family Affair Catering</h4></a></li>
            <li><a href="#personal-touch" data-toggle="tab"><h4>Mary Ellen’s Personal Touch Catering</h4></a></li>
          </ul>
        </div>
        <div class="tan-background col-lg-9 col-md-9 col-sm-9 col-xs-12">
            <!-- Tab panes -->
            <div class="tab-content">
              <div class="tab-pane active" id="precious-family">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                  <a class="thumbnail">
                    <img src="../img/about/alicia-g.jpeg" alt="Alicia Gordan">
                  </a>
                  <h3 class="name">Alicia Gordon</h3>
                  <p></p>
                  <p></p>
                  <p class="contact-info">404.771.0976</p>
                  <img class="img-responsive" src="../img/catering/alicia/sersafe.png" alt="">
                </div>
                <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                  <div class="row masonry-container">
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/1.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/2.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/3.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/4.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/5.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/6.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/7.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/8.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/9.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/10.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/11.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/12.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/13.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/14.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/15.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/16.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/17.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/18.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/19.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/20.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/21.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/22.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/23.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/24.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/25.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/26.png" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/27.png" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/28.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/29.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/30.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/31.png" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/32.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/33.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/34.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/35.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/36.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/37.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/38.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/39.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/40.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/41.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/42.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/43.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/44.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/45.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/46.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/catering/alicia/47.jpg" alt="">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="personal-touch">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                  <a class="thumbnail">
                    <img src="../img/catering/mary/2448.jpg" alt="Mary Ellen">
                  </a>
                  <h3 class="name">Mary Ellen</h3>
                  <p></p>
                  <p></p>
                  <p class="contact-info">770.316.6071</p>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                  <div class="col-md-4 col-sm-6 item">
                    <div class="thumbnail">
                      <img src="../img/catering/mary/eclaire.jpg" alt="">
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-6 item">
                    <div class="thumbnail">
                      <img src="../img/catering/mary/shrimp.jpg" alt="">
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-6 item">
                    <div class="thumbnail">
                      <img src="../img/catering/mary/veggies.jpg" alt="">
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-6 item">
                    <div class="thumbnail">
                      <img src="../img/catering/mary/fruit.jpg" alt="">
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-6 item">
                    <div class="thumbnail">
                      <img src="../img/catering/mary/meat.png" alt="">
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-6 item">
                    <div class="thumbnail">
                      <img src="../img/catering/mary/pasta.jpg" alt="">
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-6 item">
                    <div class="thumbnail">
                      <img src="../img/catering/mary/01351.jpg" alt="">
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-6 item">
                    <div class="thumbnail">
                      <img src="../img/catering/mary/2443.jpg" alt="">
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-6 item">
                    <div class="thumbnail">
                      <img src="../img/catering/mary/2447.jpg" alt="">
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-6 item">
                    <div class="thumbnail">
                      <img src="../img/catering/mary/2445.jpg" alt="">
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-6 item">
                    <div class="thumbnail">
                      <img src="../img/catering/mary/2461.jpg" alt="">
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-6 item">
                    <div class="thumbnail">
                      <img src="../img/catering/mary/135.jpg" alt="">
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-6 item">
                    <div class="thumbnail">
                      <img src="../img/catering/mary/136.jpg" alt="">
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-6 item">
                    <div class="thumbnail">
                      <img src="../img/catering/mary/137.jpg" alt="">
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
    <div id="push"></div>
  <?php include_once("footer.php");?>
