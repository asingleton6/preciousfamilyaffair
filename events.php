<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>A Precious Affair of Families</title>

  <?php include_once("header.php");?>
  <!-- Wrap all page content for sticky footer to work -->
  <div id="wrap">
    <!-- Page content -->
    <div class="">
      <div class="container adjust-pad events">
        <div class="row">
          <div class="col-lg-4 col-sm-2"></div>
          <div class="col-lg-4 col-sm-8 title"><h1>EVENTS</h1></div>
          <div class="col-lg-4 col-sm-1"></div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row event-margin-bottom">
        <div class="col-lg-5 col-sm-4 col-xs-12">
          <div class="event-date">
            <p class="event-day">
              6.24
            </p>
            <p class="event-year">
              2017
            </p>
          </div>
        </div>
        <div class="col-lg-1"></div>
        <div class="col-lg-5 col-sm-8 col-xs-12">
          <p class="event-weekday">
            Saturday
          </p>
          <p class="event-location">
            Open House Mixer
          </p>
          <p>

          </p>
          <p class="event-description">
            This is what we are doing at the meeting and what we will talk about.
          </p>
        </div>
        <div class="col-lg-1"></div>
      </div>
      <div class="row event-margin-bottom">
        <div class="col-lg-5 col-sm-4 col-xs-12">
          <div class="event-date">
            <p class="event-day">
              9.16
            </p>
            <p class="event-year">
              2017
            </p>
          </div>
        </div>
        <div class="col-lg-1"></div>
        <div class="col-lg-5 col-sm-8 col-xs-12">
          <p class="event-weekday">
            Saturday
          </p>
          <p class="event-location">
            2nd Annual Brunch
          </p>
          <p>

          </p>
          <p class="event-description">
            This is what we are doing at the meeting and what we will talk about.
          </p>
        </div>
        <div class="col-lg-1"></div>
      </div>
      <div class="row event-margin-bottom">
        <div class="col-lg-5 col-sm-4 col-xs-12">
          <div class="event-date">
            <p class="event-day">
              Spring
            </p>
            <p class="event-year">
              2018
            </p>
          </div>
        </div>
        <div class="col-lg-1"></div>
        <div class="col-lg-5 col-sm-8 col-xs-12">
          <p class="event-weekday">
            TO BE ANNOUNCED
          </p>
          <p class="event-location">
            Spring Festival
          </p>
          <p>

          </p>
          <p class="event-description">
            This is what we are doing at the meeting and what we will talk about.
          </p>
        </div>
        <div class="col-lg-1"></div>
      </div>
    </div>
    <div id="push"></div>
  <?php include_once("footer.php");?>
