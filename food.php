<?php 
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>
<?php 
// Run a select query to get my letest 6 items
// Connect to the MySQL database  
include "storescript/connect_to_mysql.php"; 
$dynamic_list = "";
$sql = mysql_query("SELECT * FROM products WHERE category='Catering' AND sub_category='food' ORDER BY date_added DESC LIMIT 9");
$productCount = mysql_num_rows($sql); // count the output amount
$i = 0;

if ($productCount > 0) {
  $dynamic_list = '<table class="table">';
  while($row = mysql_fetch_array($sql)){ 
    $id = $row["id"];
    $product_name = $row["product_name"];
    $price = $row["price"];
    $date_added = strftime("%b %d, %Y", strtotime($row["date_added"]));
    if ($i % 3 == 0) {
      $dynamic_list .= 
      '<div class="col-lg-4 col-md-6 col-sm-6">
        <div class="product-wrapper">
          <img src="inventory_images/' . $id . '.jpg" alt="' . $product_name . '" width="200" height="210" border="1" />
          <br/>
          <div class="product-info">
            <h4>' . $product_name . ' - $' . $price . '</h4>
            <br/>
            <form id="form1" name="form1" method="post" action="cart.php">
              <input type="hidden" name="pid" id="pid" value="' . $id . '">
              <button type="submit" class="btn btn-default" name="button" id="button">Add To Cart</button>
            </form>
          </div>
        </div>
      </div>';
    } else {
      $dynamic_list .= 
      '<div class="col-lg-4 col-md-6 col-sm-6">
        <div class="product-wrapper">
          <img src="inventory_images/' . $id . '.jpg" alt="' . $product_name . '" width="200" height="210" />
          <br/>
          <div class="product-info">
            <h4>' . $product_name . ' - $' . $price . '</h4>
            <br/>
            <form id="form1" name="form1" method="post" action="cart.php">
              <input type="hidden" name="pid" id="pid" value="' . $id . '">
              <button type="submit" class="btn btn-default" name="button" id="button">Add To Cart</button>
            </form>
          </div>
        </div>
      </div>';
    }
    $i++; 
  }
  $dynamic_list .= '</tr></table>';
} else {
  $dynamic_list = "We have no products listed in our store yet";
}
mysql_close();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>A Precious Affair of Families</title>

  <?php include_once("header.php");?>
  <!-- Wrap all page content for sticky footer to work -->
  <div id="wrap">
    <!-- Page content -->
    <div class="container adjust-pad">
      <div class="row">
        <div class="col-lg-4 col-sm-2"></div>
        <div class="col-lg-4 col-sm-8 title"><h1>FOOD</h1></div>
        <div class="col-lg-4 col-sm-2"></div>
      </div>
      <?php echo $dynamic_list; ?> 
    </div>
    <div id="push"></div>
  <?php include_once("footer.php");?>