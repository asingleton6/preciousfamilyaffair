  <section class="footer-decoration"></section>
</div>
<!-- Footer-->
<div id="footer">
    <div class="container first-footer">
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
          <ul>
            <li>
              <h4><a href="catering.php">Catering</a></h4>
              <p><a href="catering.php">Precious Family Affair</a></p>
              <p><a href="catering.php">Mary Ellen's Personal Touch</a></p>
            </li>
          </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
          <ul>
            <li>
              <h4><a href="gifts.php">Gifts</a></h4>
              <p><a href="ornaments.php">Ornaments</a></p>
              <p><a href="jewerly.php">Organo Gold</a></p>
              <p><a href="piggy-banks.php">Piggy Banks</a></p>
            </li>
          </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
          <ul>
            <li>
              <h4><a href="services.php">Services</a></h4>
              <p><a href="services.php">Photography</a></p>
              <p><a href="services.php">Makeup Artist</a></p>
              <p><a href="services.php">Financial Support</a></p>
            </li>
          </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 contact-info">
          <ul>
            <li>
              <h4>Contacts</h4>
              <p>404-771-0976</p>
              <p>asgkee16@gmail.com</p>
              <p>P.O. Box 960547</p>
              <p class="city-line-height">Riverdale, GA 30296</p>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="col-lg-12 second-footer">
      <p class="right-side">Designed by <a href="http://ashleysingleton.com">Ashley Singleton</a></p>
    </div>
</div>
<!-- jQuery -->
  <script src="../js/jquery.js"></script>

  <!-- Bootstrap Core JavaScript -->
  <script src="../js/bootstrap.min.js"></script>

  <!-- Scrolling Nav JavaScript -->
  <script src="../js/jquery.easing.min.js"></script>
  <script src="../js/scrolling-nav.js"></script>

  <!-- Font Awesome -->
  <script src="https://use.fontawesome.com/8ca4a59963.js"></script>
  <!--- Masonry & Image Loaded -->
  <script src="https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.min.js"></script>
  <script src="https://unpkg.com/masonry-layout@4.1/dist/masonry.pkgd.min.js"></script>
  <!-- Custom Script -->
  <script src="../js/script.js"></script>
<body>
</html>
