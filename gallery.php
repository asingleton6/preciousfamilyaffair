<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>A Precious Affair of Families</title>

  <?php include_once("header.php");?>
  <!-- Wrap all page content for sticky footer to work -->
  <div id="wrap">
    <!-- Page content -->
    <div class="container adjust-pad">
      <div class="row">
        <div class="col-lg-4 col-sm-2"></div>
        <div class="col-lg-4 col-sm-8 title"><h1>GALLERY</h1></div>
        <div class="col-lg-4 col-sm-2"></div>
      </div>
    <div class="container gallery-container">
      <div role="tabpanel">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#panel-1" aria-controls="panel-1" role="tab" data-toggle="tab">Family & Friends</a></li>
          <li role="presentation"><a href="#panel-2" aria-controls="panel-2" role="tab" data-toggle="tab">First Annual Tea & Breakfast</a></li>
          <li role="presentation"><a href="#panel-3" aria-controls="panel-2" role="tab" data-toggle="tab">Mason's Market</a></li>
          <li role="presentation"><a href="#panel-4" aria-controls="panel-2" role="tab" data-toggle="tab">Body By Brooks</a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">

          <div role="tabpanel" class="tab-pane active" id="panel-1">
            <div class="row masonry-container">

              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1527.jpg" alt="">
                </div>
              </div>

              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1035.jpg" alt="">
                </div>
              </div>

              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1547.jpg" alt="">
                </div>
              </div>

              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1455.jpg" alt="">
                </div>
              </div>

              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1500.jpg" alt="">
                </div>
              </div>

              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1466.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1302.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1415.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1050.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1037.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1398.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1552.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1078.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1460.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1539.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1374.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1294.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1282.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1489.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1033.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1479.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1088.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1554.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1313.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1199.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1408.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1267.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1497.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/familyandfriends/1423.jpg" alt="">
                </div>
              </div>
            </div> <!--/.masonry-container  -->
          </div><!--/.tab-panel -->

          <div role="tabpanel" class="tab-pane" id="panel-2">
            <div class="row masonry-container">
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2465.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2467.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2475.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2469.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2464.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2473.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2513.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2450.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2476.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2477.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2478.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2479.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2480.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2481.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2482.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2483.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2451.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2489.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2484.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2488.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2485.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2487.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2490.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2491.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2492.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2493.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2494.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/4739.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2472.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2497.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2498.jpg" alt="">
                </div>
              </div><div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2499.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2500.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2501.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2502.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2503.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2505.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2506.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2507.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2508.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2509.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2510.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2516.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2511.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/teaandbreakfast/2514.jpg" alt="">
                </div>
              </div>
            </div> <!--/.masonry-container  -->
          </div><!--/.tab-panel -->

          <div role="tabpanel" class="tab-pane" id="panel-3">
            <div class="row masonry-container">
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/masonsmarket/6.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/masonsmarket/1.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/masonsmarket/2.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/masonsmarket/3.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/masonsmarket/4.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/masonsmarket/5.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/masonsmarket/7.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/masonsmarket/8.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/masonsmarket/9.jpg" alt="">
                </div>
              </div>
            </div> <!--/.masonry-container  -->
          </div><!--/.tab-panel -->

          <div role="tabpanel" class="tab-pane" id="panel-4">
            <div class="row masonry-container">
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/bodybybrooks/1.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/bodybybrooks/2.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/bodybybrooks/3.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/bodybybrooks/4.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/bodybybrooks/5.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/bodybybrooks/6.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/bodybybrooks/7.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/bodybybrooks/8.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/bodybybrooks/9.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/bodybybrooks/10.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/bodybybrooks/11.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/bodybybrooks/12.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/bodybybrooks/13.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/bodybybrooks/14.jpg" alt="">
                </div>
              </div>
              <div class="col-md-4 col-sm-6 item">
                <div class="thumbnail">
                  <img src="../img/gallery/bodybybrooks/15.jpg" alt="">
                </div>
              </div>
            </div> <!--/.masonry-container  -->
          </div><!--/.tab-panel -->
        </div> <!--/.tab-content -->
      </div> <!--/.tab-panel  -->
    </div><!-- /.container -->
  </div>
  <div id="push"></div>
  <?php include_once("footer.php");?>
