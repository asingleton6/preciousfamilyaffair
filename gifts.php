<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>A Precious Affair of Families</title>

  <?php include_once("header.php");?>
  <!-- Wrap all page content for sticky footer to work -->
  <div id="wrap">
    <!-- Page content -->
    <div class="container adjust-pad gifts">
      <div class="row">
        <div class="col-lg-4 col-sm-2"></div>
        <div class="col-lg-4 col-sm-8 title"><h1>GIFTS</h1></div>
        <div class="col-lg-4 col-sm-2"></div>
      </div>
      <div class="row gifts-margin-bottom">
        <div class="col-lg-1"></div>
        <div class="col-lg-10 ornaments">
          <a href="ornaments.php"><img class="" src="../img/gifts/ornaments.jpg"></a>
          <h2>Personalized Ornaments</h2>
        </div>
        <div class="col-lg-1"></div>
      </div>
      <div class="row gifts-margin-bottom">
        <div class="col-lg-1"></div>
        <div class="col-lg-10 ornaments">
          <a href="organo.php"><img class="" src="../img/gifts/organo-cover.jpeg"></a>
          <h2>Organo</h2>
        </div>
        <div class="col-lg-1"></div>
      </div>
      <div class="row gifts-margin-bottom">
        <div class="col-lg-1"></div>
        <div class="col-lg-10 ornaments">
          <a href="piggy-banks.php"><img class="" src="../img/gifts/piggy-bank.jpg"></a>
          <h2>Piggy Banks</h2>
        </div>
        <div class="col-lg-1"></div>
      </div>
    </div>
    <div id="push"></div>
  <?php include_once("footer.php");?>
