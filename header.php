  <!-- Bootstrap Core CSS -->
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <!-- Custom CSS -->
  <link href="../css/main.css" rel="stylesheet">
  <link href="../css/bootstrap.vertical-tabs.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
</head>
<body>
  <!-- Navigation -->
  <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header page-scroll">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-ex1-collapse" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand page-scroll" href="index.php">
            <img class="img-responsive" src="../img/logo-navbar.png" height="50px" width="67px">
          </a>
        </div>

        <div class="collapse navbar-collapse navbar-right" id="navbar-ex1-collapse">
          <ul class="nav navbar-nav main-nav">
            <li>
              <a class="page-scroll" href="index.php">Home</a>
            </li>
            <li>
              <a class="page-scroll" href="about.php">About</a>
            </li>
            <li class="dropdown">
              <a href="catering.php" class="dropdown-toggle">Catering</a>
            </li>
            <li class="dropdown">
              <a href="gifts.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gifts <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="ornaments.php">Personalized Ornaments</a></li>
                <li><a href="organo.php">Organo Gold</a></li>
                <li><a href="piggy-banks.php">Piggy Banks</a></li>
              </ul>
            </li>
            <li>
              <a href="services.php">Services</a>
            </li>
            <li>
              <a href="gallery.php">Gallery</a>
            </li>
            <li>
              <a href="events.php">Events</a>
            </li>
            <li>
              <a class="nav-cart" href="cart.php"><i class="fa fa-cart-plus fa-2x" aria-hidden="true"></i></a>
            </li>
          </ul>
        </div>
        <!-- /.navbar-collapse -->
      </div>
      <!-- /.container -->
  </nav>
