<?php
//Error Reporting
// error_reporting(E_ALL);
// ini_set('display_errors', '1');
?>
<!--
<?php
//Run a select query to get my latest 6 items.
include "storescript/connect_to_mysql.php";
$dynamic_list = "";
$sql = mysqli_query($connection, "SELECT * FROM products ORDER BY date_added DESC LIMIT 6");
$productCount = mysqli_num_rows($sql); // count the output amount
if ($productCount > 0) {
  while($row = mysql_fetch_array($sql)){
    $id = $row["id"];
    $product_name = $row["product_name"];
    $price = $row["price"];
    $date_added = strftime("%b %d, %Y", strtotime($row["date_added"]));
    $dynamic_list .= '<table width="100%" border="0" cellspacing="0" cellpadding="6">
    <tr>
    <td width="17%" valign="top"><a href="product.php?id=' . $id . '"><img style="border:#666 1px solid;" src="inventory_images/' . $id . '.jpg" alt="' . $product_name . '" width="77" height="102" border="1" /></a></td>
    <td width="83%" valign="top">' . $product_name . '<br />
    $' . $price . '<br />
    <a href="product.php?id=' . $id . '">View Product Details</a></td>
    </tr>
    </table>';
  }
} else {
  $dynamic_list = "We have no products listed in our store yet";
}
mysqli_close();
?>
-->
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>A Precious Affair of Families</title>
  <?php include_once("header.php");?>
  <!-- Wrap all page content for sticky footer to work -->
  <div id="wrap">
    <!-- Page content -->
    <div class="logo-section">
      <img class="img-responsive" src="../img/header.png">
    </div>
    <div class="row about-section">
      <div class="container">
        <div class="col-lg-12">
            <h1 class="about-title">OUR MISSION</h1>
            <div class="about-info">
              <p>
                Family is more than blood relatives that you speak to occasionally. Family consist of, friends, co-workers, classmates; people you form life bonds with. People that you know have your back and who you support in the same way.<br /><br /></p>
              <p>
                <strong>A Precious Affair of Families</strong> is a unique, valuable company made up of people that are not only my family but my friends coming together to provide experience, services and precious gifts of memories to everyone that desire them. It was formed to be the one place that people could come and meet ordinary, everyday entrepreneurs and build lifelong relationships. I trust in the people I support on my website and I believe in the products or services they provide.<br /><br /></p>
              <p class="info-large-text">
                <strong>Come and be a part of our family…</strong><br /><br /></p>
            </div>
          </div>
        </div>
    </div>
      <!-- Intro Section -->
      <div class="row panel" id="catering">
        <div class="container">
          <div class="col-lg-6 col-sm-12 pic-padding">
            <div id="cateringCarousel" class="carousel" data-ride="carousel" data-interval="false">
              <!-- Indicators -->
              <ol class="">
                <li data-target="#cateringCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#cateringCarousel" data-slide-to="1"></li>
                <li data-target="#cateringCarousel" data-slide-to="2"></li>
                <li data-target="#cateringCarousel" data-slide-to="3"></li>
              </ol>

              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">
                <div class="item active">
                  <img src="../img/home/catering6.jpg" alt="setup">
                </div>

                <div class="item">
                  <img src="../img/home/catering3.jpg" alt="chicken">
                </div>

                <div class="item">
                  <img src="../img/home/catering2.jpg" alt="desserts">
                </div>

                <div class="item">
                  <img src="../img/home/catering5.jpg" alt="desserts">
                </div>
              </div>

              <!-- Left and right controls -->
              <a class="left carousel-control" href="#cateringCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#cateringCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
          </div>
          <div class="col-lg-6 col-sm-12">
            <h1>Let us Cater</h1>
            <h2>your Next Event</h2>
            <div class="left-margin">
              <p>If Best is Better, Good Is Not Enough!  We can promise and guarantee the food is of great quality and is as PERFECT as it could be!  Please ask about ingredients in our food to prevent any allergic reactions to anything.  We can cater small intimate parties of 2, engagements, weddings, graduations or large corporate functions.  We can come to your home, office, park or MOST other places. Please be advised, this is 2 different catering companies; A Precious Affair of Families and Mary Ellen’s Personal Touch Catering.</p>
              <button type="submit" class="btn btn-default" onclick="location.href = 'catering.php';">See More</button>
            </div>
          </div>
        </div>
      </div>
      <div class="row panel" id="gifts">
        <div class="container">
          <div class="col-lg-6 col-sm-12">
            <h2>Give your Loved One</h2>
            <h1>a Special Gift</h1>
            <div class="left-margin">
              <p>God gave everyone a gift!  It’s up to us to learn what the gift is and use it to the best of our ability. In this section are specialty gifts from jewelry, body oils, candles, t-shirts, etc. We also have personalized ornaments, piggy banks, chairs and ceramics.  Some personalized gifts have to be made and personalized, so it could take 24 hours or more before you receive your item(s)! The vendor will let you know the process. </p>
              <button type="submit" class="btn btn-default" onclick="location.href = 'gifts.php';">See More</button>
            </div>
          </div>
          <div class="col-lg-6 col-sm-12 pic-padding">
            <div id="giftsCarousel" class="carousel" data-ride="carousel" data-interval="false">
              <!-- Indicators -->
              <ol class="">
                <li data-target="#giftsCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#giftsCarousel" data-slide-to="1"></li>
                <li data-target="#giftsCarousel" data-slide-to="2"></li>
                <li data-target="#giftsCarousel" data-slide-to="3"></li>
              </ol>

              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">
                <div class="item active">
                  <img src="../img/gifts/gifts1.jpg" alt="papa">
                </div>

                <div class="item">
                  <img src="../img/gifts/gifts2.jpg" alt="crosses">
                </div>

                <div class="item">
                  <img src="../img/gifts/gifts3.jpg" alt="people">
                </div>
              </div>

              <!-- Left and right controls -->
              <a class="left carousel-control" href="#giftsCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#giftsCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="row panel" id="services">
        <div class="container">
          <div class="col-lg-6 pic-padding">
            <div id="servicesCarousel" class="carousel" data-ride="carousel" data-interval="false">
              <!-- Indicators -->
              <ol class="">
                <li data-target="#servicesCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#servicesCarousel" data-slide-to="1"></li>
                <li data-target="#servicesCarousel" data-slide-to="2"></li>
                <li data-target="#servicesCarousel" data-slide-to="3"></li>
              </ol>

              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">
                <div class="item active">
                  <img src="http://cdn3.craftsy.com/blog/wp-content/uploads/2015/11/8QFP76HMEK.jpg" alt="Chania">
                </div>
              </div>

              <!-- Left and right controls -->
              <a class="left carousel-control" href="#servicesCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#servicesCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
          </div>
          <div class="col-lg-6">
          <h2>Become Apart</h2>
          <h1>of the Family</h1>
          <div class="left-margin">
            <p>A group becomes a team when each member is sure enough of himself… to praise the skills of others by Norman Shide.  The color blue is my company main color. It symbolizes trust, loyalty, wisdom, confidence, intelligence, faith and heaven. The following services are from people whom have studied and mastered what they offer to you.  We have a range of other services Web Designer, Financial Assistant/Tax Preparation, Photography, Hair Stylist, Realtor and Make-Up Artist</p>
            <button type="submit" class="btn btn-default" onclick="location.href = 'services.php';">See More</button>
          </div>
        </div>
        </div>
      </div>
      <?php include_once("footer.php");?>
