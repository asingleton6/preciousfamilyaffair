/* Demo Scripts for Making Twitter Bootstrap 3 Tab Play Nicely With The Masonry Library
 * on SitePoint by Maria Antonietta Perna
 */

//Initialize Masonry inside Bootstrap 3 Tab component
var $container = $('.masonry-container');
$container.imagesLoaded(function() {
  $container.masonry({
    columnWidth: '.item',
    itemSelector: '.item'
  });
});

//Reinitialize masonry inside each panel after the relative tab link is clicked -
$('a[data-toggle=tab]').each(function() {
  var $this = $(this);

  $this.on('shown.bs.tab', function() {

    $container.imagesLoaded(function() {
      $container.masonry({
        columnWidth: '.item',
        itemSelector: '.item'
      });
    });

  }); //end shown
}); //end each


jQuery(function($) {
  if ($(window).width() > 769) {
    $('.navbar .dropdown').hover(function() {
      $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();

    }, function() {
      $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();

    });

    $('.navbar .dropdown > a').click(function() {
      location.href = this.href;
    });
  }
});

// Highlight the active navigation item
$(document).ready(function() {
  $('main-nav a').click(function() {
    $('main-nav a').removeClass("active-main");
    $(this).addClass("active-main");
  });
});
