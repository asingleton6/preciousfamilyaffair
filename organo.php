<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>A Precious Affair of Families</title>

  <?php include_once("header.php");?>
  <!-- Wrap all page content for sticky footer to work -->
  <div id="wrap">
    <!-- Page content -->
    <div class="container adjust-pad gifts" id="organo">
      <div class="row">
        <div class="col-lg-4 col-sm-2"></div>
        <div class="col-lg-4 col-sm-8 title"><h1>ORGANO GOLD</h1></div>
        <div class="col-lg-4 col-sm-2"></div>
      </div>
      <div class="row margin-bottom">
        <div class="col-lg-6 col-md-12">
          <img class="img-responsive" src="../img/gifts/organo/org1.jpg">
        </div>
        <div class="col-lg-6 col-md-12">
          <h2>7 DAY CHALLENGE</h2>
          <p>
            7 days in a week, 7 deadly sins, 7 continents make a world, 7 colors makes a rainbow
            When a doctor prescribes a new medication they tell you to take it for a few days, if you have any problems or side effects come back or call the office.
            A newborn baby has to try one food at a time, for a few days to check for allergic reaction.
            Take my 7 day challenge and see how these products taste, how these products makes you feel, how much money you can save making great coffee and tea from your own home!!!!
          </p>
          <h3>7 Day Challenge Trial Pack</h3>
          <ul>
            <li>Café Supreme Coffee		1</li>
            <li>Dark Bold Black Coffee		2</li>
            <li>Smooth Creamy Latte		2</li>
            <li>Café Mocha				1</li>
            <li>Hot Chocolate			1</li>
            <li>Green Tea The Vert		2</li>
            <li>Red Tea The Rouge		1</li>
          </ul>
          <br />
          <p>
            When doing the 7 day challenge try not to drink any other coffee or tea products that you would normally make at home or buy from a restaurant. You want to be able to get all the flavors and taste from these products?  You want to see how these products make you feel? You want to see if you are allergic or have a reaction to any ingredients in the products?
            <br />
            <br />
            The trial packs are $22.99, includes tax, shipping and handling!!!!
          </p>
          <br />
          <p>
            Thank You for trying out Organo Gold and taking my 7 Day Challenge...<br />
            ALICIA S. GORDON<br />
            INDEPENDENT DISTRIBUTOR<br />
            404-771-0976<br />
            To order additional products <a href="http://www.preciousproducts16.organogold.com/">visit my page</a>.<br />
          </p>
        </div>
      </div>
    </div>
    <div id="push"></div>
  <?php include_once("footer.php");?>
