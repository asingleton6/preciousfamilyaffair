<?php 
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>
<?php 
include "storescript/connect_to_mysql.php";  
//Check to see the URL variable is set and that it existing in the database.
if(isset($_GET['id'])) {
  $id = preg_replace('#[^0-9]#i', "", $_GET['id']);
  //Use this var to check to see if this ID exist, if yes then get the product
  //details, if no then exit this script and give message why.
  $sql = mysql_query("SELECT * FROM products WHERE id='$id' LIMIT 1");
  $product_count = mysql_num_rows($sql); // count the output amount
  if ($product_count > 0) {
    //Get all the product details
    while($row = mysql_fetch_array($sql)){ 
      $product_name = $row["product_name"];
      $price = $row["price"];
      $details = $row["details"];
      $category = $row["category"];
      $subcategory = $row["subcategory"];
      $date_added = strftime("%b %d, %Y", strtotime($row["date_added"]));
    }

  } else {
    echo "That item does not exist";
    exit();
  }
} else {
  echo "Data to render this page is missing";
  exit();
}
mysql_close();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?php echo $product_name; ?></title>

  <?php include_once("header.php");?>
  <!-- Wrap all page content for sticky footer to work -->
  <div id="wrap">
    <!-- Page content -->
    <div class="container adjust-width-others"> 
      <img src="inventory_images/<?php echo $id; ?>.jpg" width="210" height="200" alt="<?php echo $product_name; ?>"><br>
      <a href="inventory_images/<?php echo $id; ?>.jpg">View Full Size Image</a> 
      <p><?php echo $product_name; ?></p>
      <p><?php echo "$".$price; ?></p>
      <p><?php echo $details; ?></p>
      <form id="form1" name="form1" method="post" action="cart.php">
        <input type="hidden" name="pid" id="pid" value="<?php echo $id; ?>">
        <input type="submit" name="button" id="button" value="Add to Shopping Cart">
      </form>
    </div>
    <div id="push"></div>
    <section class="wood-texture"></section>
  </div>
  <?php include_once("footer.php");?>