<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>A Precious Affair of Families</title>

  <?php include_once("header.php");?>
  <!-- Wrap all page content for sticky footer to work -->
  <div id="wrap">
    <!-- Page content -->
    <div class="container adjust-pad">
      <div class="row">
        <div class="col-lg-4 col-sm-2"></div>
        <div class="col-lg-4 col-sm-8 title"><h1>SERVICES</h1></div>
        <div class="col-lg-4 col-sm-2"></div>
      </div>
      <div id="contact" class="row">
        <div class="col-lg-12">
          <p>
            Are you an Entrepreneur?
            Do you have a Website?
            Do you have a small business?
            How do you advertise your business?
            How do you find customers to support your business?
          </p>
          <p>
            Come and be a part of a growing family of Entrepreneurs!!!!!
          </p>
          <p>

          </p>
          <p>
            Please email me Alicia S. Gordon and find out how to become one of us! Be advised, this website can only have 2 businesses in the same category.  So look up the service you provide and see if all the slots are filled. Not all services are listed more services can be provided through this website! Please send the following information to asgkee16@gmail.com! You will be contacted within 24 – 48 hours……
          </p>
          <strong>
            Your Name,
            Phone Number,
            Type of Business,
            Products you Sell,
            Do you have Business Licenses or Tax ID NUMBER?
          </strong>
        </div>
      </div>
      <div class="row services">
        <div class="white-background col-lg-3 col-md-3 col-sm-3 col-xs-12"> <!-- required for floating -->
          <!-- Nav tabs -->
          <ul class="nav nav-tabs tabs-left">
            <li class="active"><a href="#photography" data-toggle="tab"><h4>Photography</h4></a></li>
            <li><a href="#organo" data-toggle="tab"><h4>Organo Gold</h4></a></li>
            <li><a href="#web-developer" data-toggle="tab"><h4>Web Developer</h4></a></li>
            <li><a href="#hair-stylist" data-toggle="tab"><h4>Beautician & Barber</h4></a></li>
            <li><a href="#financial-support" data-toggle="tab"><h4>Financial Support</h4></a></li>
            <li><a href="#realtor" data-toggle="tab"><h4>Realtor</h4></a></li>
            <li><a href="#fitness-trainer" data-toggle="tab"><h4>Fitness Trainer</h4></a></li>
            <li><a href="#sales-consultant" data-toggle="tab"><h4>Sales Consultant</h4></a></li>
            <li><a href="#notary" data-toggle="tab"><h4>Notary Public</h4></a></li>
            <li><a href="#cleaning-supplies" data-toggle="tab"><h4>Cleaning Supplies</h4></a></li>
            <li><a href="#salesperson" data-toggle="tab"><h4>Salesperson</h4></a></li>
            <li><a href="#truck-services" data-toggle="tab"><h4>Truck Services</h4></a></li>
          </ul>
        </div>
        <div class="tan-background col-lg-9 col-md-9 col-sm-9 col-xs-12">
            <!-- Tab panes -->
            <div class="tab-content">
              <div class="tab-pane active" id="photography">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                  <a class="thumbnail">
                    <img src="../img/services/trivis/trivis.jpg" alt="Trivis Houston">
                  </a>
                  <h3 class="name">Trivis Houston</h3>
                  <p>I'm a creative soul that seeks out beauty in my everyday life. I'm inspired to shoot because of the real moments I capture behind my lens. I want to preserve those memories you cherish for years to come. So let me share what I enjoy doing most, being behind the lens capturing the beauty of those cherised moments.</p>
                  <p>Feel free to contact me at photosbytrivis@gmail.com for any questions or inquires.</p>
                  <p class="contact-info"><a href="http://www.photosbytrivis.net/">www.photosbytrivis.net</a></p>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                  <div class="row masonry-container">
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/services/trivis/couple.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/services/trivis/baby-couple.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/services/trivis/gentlemen.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/services/trivis/4.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/services/trivis/1.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/services/trivis/2.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/services/trivis/3.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/services/trivis/wedding-couple.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/services/trivis/5.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/services/trivis/6.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/services/trivis/7.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/services/trivis/8.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/services/trivis/11.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/services/trivis/10.jpg" alt="">
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 item">
                      <div class="thumbnail">
                        <img src="../img/services/trivis/9.jpg" alt="">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="organo">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                  <a class="thumbnail">
                    <img src="../img/services/organo/org2.jpg" alt="">
                  </a>
                  <h3 class="name">Organo Gold</h3>
                  <p></p>
                  <p></p>
                  <p class="contact-info"><a href=""></a></p>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                  <p>
                    Visit Alicia Gordon's <a href="http://www.preciousproducts16.organogold.com/">Organo Gold</a> page to become a distributor.
                  </p>
                </div>
              </div>
              <div class="tab-pane" id="web-developer">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                  <a class="thumbnail">
                    <img src="../img/services/ashley/ashley.jpg" alt="Ashley Singleton">
                  </a>
                  <p>asingleton6@gmail.com</p>
                  <p>678.634.3069</p>
                  <p class="contact-info"><a href="http://www.ashleysingleton.com/">wwww.ashleysingleton.com</a></p>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                  <h1 class="name">Ashley Singleton</h1>
                  <p>
                    I'm a Web developer with extensive knowledge in creating mobile friendly websites. If you are a small business/statup interested in hiring me for web development services you can contact me anytime by email or phone. I hope to hear from you soon!
                  </p>
                </div>
              </div>
              <div class="tab-pane" id="hair-stylist">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                  <a class="thumbnail">
                    <img src="../img/services/hairstylist/destiny.jpg" alt="destiny">
                  </a>
                  <h3 class="name">Destiny</h3>
                  <a class="thumbnail">
                    <img src="../img/services/hairstylist/alice.jpg" alt="alice">
                  </a>
                  <h3 class="name">Alice</h3>
                  <p></p>
                  <p></p>
                  <p class="contact-info"><a href=""></a></p>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">

                </div>
              </div>
              <div class="tab-pane" id="financial-support">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                  <a class="thumbnail">
                    <img src="../img/services/financial/1538.jpg" alt="">
                  </a>
                  <h3 class="name">Janette Hines</h3>
                  <p></p>
                  <p></p>
                  <p class="contact-info"><a href=""></a></p>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">

                </div>
              </div>
              <div class="tab-pane" id="consultant">
                <div class="col-lg-5 col-md-4 col-sm-4 col-xs-12">
                  <a class="thumbnail">
                    <img src="../img/services/consultant/consuela.jpg" alt="consuela">
                  </a>
                  <h3 class="name">Consuela Thompson</h3>
                  <p>Consuela Thompson is a successful professional with more than 10 years of leadership and development experience in the private, public and nonprofit sectors. She has the ability to communicate and inspire diverse communities to collaborate on bold initiatives and improve the quality of life for all stakeholders.</p>
                  <p>Knowledgeable in developing and evaluating programs, and establishing collaborative partnerships, Ms. Thompson has strong skills leading, creating, designing and maintaining programs for all ages. She possesses a sincere passion for service and a firm commitment to empower individuals, families and communities. Ms. Thompson acquired a Bachelor in Sociology from Georgia State University, and is currently pursuing a Master in Public Administration.</p>
                  <p class="contact-info"><a href=""></a></p>
                </div>
                <div class="col-lg-7 col-md-8 col-sm-8 col-xs-12">

                </div>
              </div>
              <div class="tab-pane" id="realtor">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                  <a class="thumbnail">
                    <img src="../img/services/realtor/illya.jpg" alt="">
                  </a>
                  <h3 class="name">Illya Menifee</h3>
                  <p></p>
                  <p></p>
                  <p class="contact-info"><a href=""></a></p>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">

                </div>
              </div>
              <div class="tab-pane" id="fitness-trainer">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                  <a class="thumbnail">
                    <img src="http://placehold.it/500x500" alt="">
                  </a>
                  <h3 class="name">Trainer</h3>
                  <p></p>
                  <p></p>
                  <p class="contact-info"><a href=""></a></p>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                  <h2>Available Slots</h2>
                  <p>2</p>
                </div>
              </div>
              <div class="tab-pane" id="sales-consultant">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                  <a class="thumbnail">
                    <img src="http://placehold.it/500x500" alt="">
                  </a>
                  <h3 class="name">Sales Consultant</h3>
                  <p></p>
                  <p></p>
                  <p class="contact-info"><a href=""></a></p>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                  <h2>Available Slots</h2>
                  <p>2</p>
                </div>
              </div>
              <div class="tab-pane" id="notary">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                  <a class="thumbnail">
                    <img src="../img/services/notary/notary-public.jpg" height="500px" width="500px" alt="">
                  </a>
                  <p></p>
                  <p></p>
                  <p class="contact-info"><a href=""></a></p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                  <h3 class="name">Alicia Gordon</h3>
                  <h3 class="name">Trivis Houston</h3>
                  <h3 class="name">Janette Hines</h3>
                </div>
              </div>
              <div class="tab-pane" id="cleaning-supplies">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                  <a class="thumbnail">
                    <img src="http://placehold.it/500x500" alt="">
                  </a>
                  <h3 class="name">Cleaning Supplies</h3>
                  <p></p>
                  <p></p>
                  <p class="contact-info"><a href=""></a></p>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                  <h2>Available Slots</h2>
                  <p>2</p>
                </div>
              </div>
              <div class="tab-pane" id="salesperson">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                  <a class="thumbnail">
                    <img src="http://placehold.it/500x500" alt="">
                  </a>
                  <h3 class="name">Salesperson</h3>
                  <p></p>
                  <p></p>
                  <p class="contact-info"><a href=""></a></p>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                  <h2>Available Slots</h2>
                  <p>2</p>
                </div>
              </div>
              <div class="tab-pane" id="truck-services">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                  <a class="thumbnail">
                    <img src="http://placehold.it/500x500" alt="">
                  </a>
                  <h3 class="name">Truck Services</h3>
                  <p></p>
                  <p></p>
                  <p class="contact-info"><a href=""></a></p>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                  <h2>Available Slots</h2>
                  <p>2</p>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
    <div id="push"></div>
  <?php include_once("footer.php");?>
