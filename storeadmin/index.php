<?php include_once("check_session.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Admin Management</title>

  <?php include_once("../header.php");?>
  <!-- Wrap all page content for sticky footer to work -->
  <div id="wrap">
    <!-- Page content -->
    <div class="container adjust-pad">
      <br>
      <br>
      <br>
      <br>
      <h1>Administrative Console</h1>
      <a href="inventory_list.php">Manage Inventory</a>
    </div>
    <div id="push"></div>
    <section class="wood-texture"></section>
  </div>
  <?php include_once("../footer.php");?>