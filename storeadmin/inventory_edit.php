<?php include_once("check_session.php");?>
<?php 
// Script Error Reporting
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>
<?php 
// Parse the form data and add inventory item to the system
if (isset($_POST['product_name'])) {
  
  $pid = mysql_real_escape_string($_POST['thisID']);
    $product_name = mysql_real_escape_string($_POST['product_name']);
  $price = mysql_real_escape_string($_POST['price']);
  $category = mysql_real_escape_string($_POST['category']);
  $sub_category = mysql_real_escape_string($_POST['sub_category']);
  $details = mysql_real_escape_string($_POST['details']);
  // See if that product name is an identical match to another product in the system
  $sql = mysql_query("UPDATE products SET product_name='$product_name', price='$price', details='$details', category='$category', sub_category='$sub_category' WHERE id='$pid'");
  if ($_FILES['fileField']['tmp_name'] != "") {
      // Place image in the folder 
      $newname = "$pid.jpg";
      move_uploaded_file($_FILES['fileField']['tmp_name'], "../inventory_images/$newname");
  }
  header("location: inventory_list.php"); 
  exit();
}
?>
<?php 
// Gather this product's full information for inserting automatically into the edit form below on page
if (isset($_GET['pid'])) {
  $targetID = $_GET['pid'];
    $sql = mysql_query("SELECT * FROM products WHERE id='$targetID' LIMIT 1");
    $productCount = mysql_num_rows($sql); // count the output amount
    if ($productCount > 0) {
      while($row = mysql_fetch_array($sql)){ 
             
       $product_name = $row["product_name"];
       $price = $row["price"];
       $category = $row["category"];
       $sub_category = $row["sub_category"];
       $details = $row["details"];
       $date_added = strftime("%b %d, %Y", strtotime($row["date_added"]));
        }
    } else {
      echo "Sorry dude that crap doesn't exist.";
      exit();
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Add New Inventory Item Form</title>

  <?php include_once("../header.php");?>
  <!-- Wrap all page content for sticky footer to work -->
  <div id="wrap">
    <!-- Page content -->
    <div class="container adjust-pad">
      <div><a href="inventory_list.php#inventory_form">+ Add New Inventory Item</a></div>
      <h1>Manage Inventory</h1>
      <a name="inventory_form" id="inventor_form"></a>
      <h3>Add New Inventory Item Form</h3>
      <form action="inventory_edit.php" enctype="multipart/form-data" name="my_form" id="my_form" method="post">
        <table class="table">
          <thead>
            <tr>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Product Name</td>
              <td><input type="text" id="product_name" name="product_name" size="64" value="<?php echo $product_name; ?>"></td>
            </tr>
            <tr>
              <td>Product Price</td>
              <td><input type="text" id="price" name="price" placeholder="$" size="64" value="<?php echo $price; ?>"></td>
            </tr>
            <tr>
              <td>Category</td>
              <td>
                <select id="category" name="category">
                  <option value="<?php echo $category; ?>"><?php echo $category; ?></option>
                  <option>Catering</option>
                  <option>Custom Gifts</option>
                  <option>Over Services</option>
                </select>
              </td>
            </tr>
            <tr>
              <td>Sub Category</td>
              <td>
                <select id="sub_category" name="sub_category">
                  <option value="<?php echo $sub_category; ?>"><?php echo $sub_category; ?></option>
                  <option>Food</option>
                  <option>Beverage</option>
                  <option>Desserts</option>
                  <option>Baked Goods</option>
                  <option>Personalized Ornaments</option>
                  <option>Piggy Banks</option>
                  <option>Jewelry</option>
                  <option>Photography</option>
                  <option>Makeup Artist</option>
                  <option>Financial Support</option>
                </select>
              </td>
            </tr>
            <tr>
              <td>Product Details</td>
              <td><textarea type="file" name="details" id="details" rows="5" size="64"><?php echo $details; ?></textarea></td>
            </tr>
            <tr>
              <td>Product Image</td>
              <td><input type="file" name="fileField" id="fileField" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp"></td>
            </tr>
            <tr>
              <td><input name="thisID" type="hidden" value="<?php echo $targetID; ?>" /></td>
              <td><button type="submit" id="button" name="button" class="btn btn-primary">Submit Changes</button></td>
            </tr>
          </tbody>
        </table>
      </form>
    </div>
    <div id="push"></div>
    <section class="wood-texture"></section>
  </div>
  <?php include_once("../footer.php");?>