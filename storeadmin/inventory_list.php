<?php include_once("check_session.php");?>
<?php  
//Error Reporting
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>
<?php 
// Delete Item Question to Admin, and Delete Product if they choose
if (isset($_GET['deleteid'])) {
  echo 'Do you really want to delete product with ID of ' . $_GET['deleteid'] . '? <a href="inventory_list.php?yesdelete=' . $_GET['deleteid'] . '">Yes</a> | <a href="inventory_list.php">No</a>';
  exit();
}
if (isset($_GET['yesdelete'])) {
  // remove item from system and delete its picture
  // delete from database
  $id_to_delete = $_GET['yesdelete'];
  $sql = mysql_query("DELETE FROM products WHERE id='$id_to_delete' LIMIT 1") or die (mysql_error());
  // unlink the image from server
  // Remove The Pic -------------------------------------------
    $pictodelete = ("../inventory_images/$id_to_delete.jpg");
    if (file_exists($pictodelete)) {
              unlink($pictodelete);
    }
  header("location: inventory_list.php"); 
    exit();
}
?>
<?php 
// Parse the form data and add inventory item to the system
if (isset($_POST['product_name'])) {
  $product_name = mysql_real_escape_string($_POST['product_name']);
  $price = mysql_real_escape_string($_POST['price']);
  $category = mysql_real_escape_string($_POST['category']);
  $sub_category = mysql_real_escape_string($_POST['sub_category']);
  $details = mysql_real_escape_string($_POST['details']);
  // See if that product name is an identical match to another product in the system
  $sql = mysql_query("SELECT id FROM products WHERE product_name='$product_name' LIMIT 1");
  $productMatch = mysql_num_rows($sql); // count the output amount
    if ($productMatch > 0) {
    echo 'Sorry you tried to place a duplicate "Product Name" into the system, <a href="inventory_list.php">click here</a>';
    exit();
  }
  // Add this product into the database now
  $sql = mysql_query("INSERT INTO products (product_name, price, details, category, sub_category, date_added) 
        VALUES('$product_name','$price','$details','$category','$sub_category',now())") or die (mysql_error());
     $pid = mysql_insert_id();
  // Place image in the folder 
  $newname = "$pid.jpg";
  move_uploaded_file( $_FILES['fileField']['tmp_name'], "../inventory_images/$newname");
  header("location: inventory_list.php"); 
    exit();
}
?>
<?php 
// This block grabs the whole list for viewing
$product_list = "";
$sql = mysql_query("SELECT * FROM products ORDER BY date_added DESC");
$productCount = mysql_num_rows($sql); // count the output amount
if ($productCount > 0) {
  while($row = mysql_fetch_array($sql)){ 
             $id = $row["id"];
       $product_name = $row["product_name"];
       $price = $row["price"];
       $date_added = strftime("%b %d, %Y", strtotime($row["date_added"]));
       $product_list .= "Product ID: $id - <strong>$product_name</strong> - $$price - <em>Added $date_added</em> &nbsp; &nbsp; &nbsp; <a href='inventory_edit.php?pid=$id'>edit</a> &bull; <a href='inventory_list.php?deleteid=$id'>delete</a><br />";
    }
} else {
  $product_list = "You have no products listed in your store yet";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Inventory List</title>

  <?php include_once("../header.php");?>
  <!-- Wrap all page content for sticky footer to work -->
  <div id="wrap">
    <!-- Page content -->
    <div class="container adjust-pad">
      <div><a href="inventory_list.php#inventory_form">+ Add New Inventory Item</a></div>
      <h1>Manage Inventory</h1>
      <?php echo $product_list; ?>
      <a name="inventory_form" id="inventor_form"></a>
      <h3>Add New Inventory Item Form</h3>
      <form action="inventory_list.php" enctype="multipart/form-data" name="my_form" id="my_form" method="post">
        <table class="table">
          <thead>
            <tr>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Product Name</td>
              <td><input type="text" id="product_name" name="product_name" size="64"></td>
            </tr>
            <tr>
              <td>Product Price</td>
              <td><input type="text" id="price" name="price" placeholder="$" size="64"></td>
            </tr>
            <tr>
              <td>Category</td>
              <td>
                <select id="category" name="category">
                  <option>Catering</option>
                  <option>Custom Gifts</option>
                  <option>Over Services</option>
                </select>
              </td>
            </tr>
            <tr>
              <td>Sub Category</td>
              <td>
                <select id="sub_category" name="sub_category">
                  <option>Food</option>
                  <option>Beverage</option>
                  <option>Desserts</option>
                  <option>Baked Goods</option>
                  <option>Personalized Ornaments</option>
                  <option>Piggy Banks</option>
                  <option>Jewelry</option>
                  <option>Photography</option>
                  <option>Makeup Artist</option>
                  <option>Financial Support</option>
                </select>
              </td>
            </tr>
            <tr>
              <td>Product Details</td>
              <td><textarea type="file" name="details" id="details" rows="5" size="64"></textarea></td>
            </tr>
            <tr>
              <td>Product Image</td>
              <td><input type="file" name="fileField" id="fileField" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp"></td>
            </tr>
            <tr>
              <td></td>
              <td><button type="submit" id="button" name="button" class="btn btn-primary">Submit</button></td>
            </tr>
          </tbody>
        </table>
      </form>
    </div>
    <div id="push"></div>
    <section class="wood-texture"></section>
  </div>
  <?php include_once("../footer.php");?>