<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>A Precious Affair of Families</title>

  <!-- Bootstrap Core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="css/main.css" rel="stylesheet">
</head>
<body>
  <!-- Navigation -->
  <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
      <div class="navbar-header page-scroll">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand page-scroll" href="index.html"><img src="img/logo.png" alt width="79px"></a>
      </div>

      <div class="collapse navbar-collapse navbar-ex1-collapse navbar-right">
        <ul class="nav navbar-nav">
          <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
          <li class="hidden">
            <a class="page-scroll" href="page-top"></a>
          </li>
          <li>
            <a class="page-scroll" href="index.html">Home</a>
          </li>
          <li>
            <a class="page-scroll" href="about.html">About</a>
          </li>
          <li class="dropdown">
            <a href="catering.html" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Catering <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="food.html">Food</a></li>
              <li><a href="#">Beverages</a></li>
              <li><a href="#">Baked Goods</a></li>
            </ul>
          </li>
          <li class="dropdown">
            <a href="#gifts.html" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Custom Gifts <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="#">Personalized Ornaments</a></li>
              <li><a href="#">Piggy Banks</a></li>
              <li><a href="#">Jewelry</a></li>
            </ul>
          </li>
          <li class="dropdown">
            <a href="#services.html" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Other Services <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="#">Photography</a></li>
              <li><a href="#">Makeup Artist</a></li>
              <li><a href="#">Financial Support</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
  </nav>
  <!-- Wrap all page content for sticky footer to work -->
  <div id="wrap">
    <!-- Page content -->
    <div class="container">
      
    </div>
    <div id="push"></div>
    <section class="wood-texture"></section>
  </div>
  <!-- Footer-->
  <div id="footer">
    <div class="container first-footer">
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <ul>
            <li>
              <h4><a href="#catering.html">Catering</a></h4>
              <h5><a href="#food.html">Food</a></h5>
              <h5><a href="#beverages.html">Beverages</a></h5>
              <h5><a href="#baked-goods.html">Baked Goods</a></h5>
            </li>
          </ul>
        </div>
        <div class="col-lg-3 col-xs-6">
          <ul>
            <li>
              <h4><a href="#gits.html">Custom Gifts</a></h4>
              <h5><a href="#ornaments.html">Personalized Ornaments</a></h5>
              <h5><a href="#piggy-banks.html">Piggy Banks</a></h5>
              <h5><a href="#jewerly.html">Jewerly</a></h5>
            </li>
          </ul>
        </div>
        <div class="col-lg-3 col-xs-6">
          <ul>
            <li>
              <h4><a href="#services.html">Other Services</a></h4>
              <h5><a href="#photography.html">Photography</a></h5>
              <h5><a href="#makeup-artist.html">Makeup Artist</a></h5>
              <h5><a href="#financial.html">Financial Support</a></h5>
            </li>
          </ul>
        </div>
        <div class="col-lg-3 col-xs-6">
          <ul>
            <li>
              <h4><a href="#"><i class="fa fa-phone" aria-hidden="true"></i> 888-888-8888</a></h4>
              <h5><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i> info@email.com</a></h5>
              <h5><a href="#"><i class="fa fa-home" aria-hidden="true"></i> Monday - Friday</a></h5>
              <h5><a href="#"><i class="fa fa-clock-o" aria-hidden="true"></i> 9:00 AM - 5 PM</a></h5>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="col-lg-12 second-footer">
      <p class="right-side">Designed by <a href="http://ashleysingleton.com">Ashley Singleton</a></p>
    </div>
  </div>
  <!-- jQuery -->
  <script src="js/jquery.js"></script>

  <!-- Bootstrap Core JavaScript -->
  <script src="js/bootstrap.min.js"></script>

  <!-- Scrolling Nav JavaScript -->
  <script src="js/jquery.easing.min.js"></script>
  <script src="js/scrolling-nav.js"></script>

  <!-- Font Awesome -->
  <script src="https://use.fontawesome.com/8ca4a59963.js"></script>
<body>
</html>