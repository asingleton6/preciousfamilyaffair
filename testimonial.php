<div class="row panel" id="testimonial">
    <div class="col-lg-12">
      <div class="review-padding">
        <h2>Customer Reviews</h2>
      </div>
      <div class="carousel-reviews broun-block">
        <div id="carousel-reviews" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <div class="item active">
              <div class="col-md-4 col-sm-6">
                <div class="block-text">
                  <a title="" href="#">Fall over dead</a>
                  <p>"Chew foot. Eat grass, throw it back up. Licks paws sit and stare meow meow, i tell my human but use lap as chair hunt anything that moves, for my left donut is missing, as is my right for run outside as soon as door open. Stare at wall turn and meow stare at wall some more meow again continue staring meowing non stop for food. Lies down plan steps for world domination and stares at human while pushing stuff off a table..."</p>
                </div>
              </div>
              <div class="col-md-4 col-sm-6">
                <div class="block-text">
                  <a title="" href="#">Lick arm hair</a>
                  <p>"Sit in window and stare ooo, a bird! yum immediately regret falling into bathtub steal the warm chair right after you get up cat is love, cat is life yet behind the couch. Step on your keyboard while you're gaming and then turn in a circle run outside as soon as door open yet lay on arms while you're using the keyboard stare out the window for thinking longingly about tuna brine."</p>
                </div>
              </div>
              <div class="col-md-4 col-sm-6">
                <div class="block-text">
                  <a title="" href="#">Meow to be let out</a>
                  <p>"Lick the other cats destroy couch as revenge. Howl uncontrollably for no reason hide when guests come over, but brown cats with pink ears, chase after silly colored fish toys around the house. Howl on top of tall thing mew, but wake up human for food at 4am licks paws cats making all the muffins or eat the fat cats food."</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<div id="push"></div>
